.. _history:

######################
Historique de versions
######################

Sont notées ici les principales évolutions pour chaque version. Le détail exhaustif de toutes les modifications est consigné dans le fichier HISTORY.txt dans le code source de l'application.

v1.14 (22/03/2024)
==================

...


v1.13 (06/12/2022)
==================

...


v1.12 (06/09/2021)
==================

...


v1.11 (04/02/2021)
==================

...


v1.10 (19/01/2021)
==================

* Un nouveau widget de tableau de bord permet de faire une recherche directe sur le code de l'établissement et d'accéder directement à la fiche de cet établissement.

* Mise en valeur des dossiers clôturés sur les listings de DC et de DI en grisant la ligne d'un enregistrement clôturé.

* Nouveaux champs de fusion dans l'édition PDF du planning de programmation de visites : nombre de visites planifiées dans cette programmation, dates du premier jour et du dernier de la semaine de programmation en toutes lettres.

* Aide à la saisie sur les champs date qui permettent désormais grâce à la touche 'Espace' de renseigner la date du jour.

* Augmentation du nombre de caractères possibles dans le nom du fichier lors d'un téléversement. La limite était de 50 caractères, elle est maintenant de 250 caractères.

* L'édition PDF du planning de programmation des visites est désormais disponible même si la programmation n'est pas encore validée.

* Améliorations du champ de fusion [analyse.prescriptions] : une ligne complète de prescriptions (ensemble réglementaire + spécifique) est maintenant insécable (ne peut pas être coupée et apparaître sur deux pages), si la prescription réglementaire ou la prescription est vide la ligne vide n'apparaît plus.

* Ajout du champ de fusion [analyse.prescriptions_unifiees] : identique au tableau des prescriptions existant mais sans la ligne séparatrice entre prescription réglemntaire et spécifique.

* La numéroration de l'ordre du jour de réunion tient désormais compte des dates de visite pour son ordre de renumérotation.

* La saisie du complément dans les essais réalisés se fait maintenant dans un champ de texte sur plusieurs lignes.

* Ajout des champs de fusion [dpr.motivation] et [dpr.resume] sur les demandes de passage en réunion.

* Nouveau critère de recherche avancée sur l'état de l'analyse dans les listings de DI.

* Nouvelle colonne présentant l'état de l'analyse dans les listings de DI.

* Nouvelle entrée de menu permettant de lister toutes les décisions d'autorités de police.

* Ajout de la colonne date de dernière visite dans le listing des propositions de visites dans l'action programmer de la programmation comme marqueur permettant d'indiquer qu'une visite est en cours sur cet établissement (dualité périodique/réception).


v1.9 (22/09/2020)
=================

* Il est désormais possible de renseigner manuellement le code de l'établissement depuis le formulaire d'ajout d'une fiche établissement. Une action dédiée 'renuméroter' est également disponible sur la fiche établissement pour permettre de modifier ce code sur un établissement déjà existant.


v1.8 (20/07/2020)
=================

...


v1.7 (23/06/2020)
=================

...


v1.6 (15/07/2019)
=================

...


v1.5 (23/11/2018)
=================

...


v1.4 (17/04/2018)
=================

...


v1.3 (21/12/2017)
=================

...


v1.2 (23/03/2017)
=================

...


v1.1 (03/03/2017)
=================

...


v1.0 (16/01/2017)
=================

* Version initiale.

